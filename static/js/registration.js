function registration()
	{

		var name= document.getElementById("name").value;
		var email= document.getElementById("email").value;
		var uname= document.getElementById("username").value;
		var pwd= document.getElementById("password").value;			
		var cpwd= document.getElementById("confirmpassword").value;
		
        //email id expression code
		var pwd_expression = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/;
		var letters = /^[A-Za-z]+$/;
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if(name=='')
		{
			alert('Please enter your name');
		}
		else if(!letters.test(name))
		{
			alert('Name field required only alphabet characters');
		}
		else if(email=='')
		{
			alert('Please enter your user email id');
		}
		else if (!filter.test(email))
		{
			alert('Invalid email');
		}
		else if(uname=='')
		{
			alert('Please enter the user name.');
		}
		else if(!letters.test(uname))
		{
			alert('User name field required only alphabet characters');
		}
		else if(pwd=='')
		{
			alert('Please enter Password');
		}
		else if(cpwd=='')
		{
			alert('Enter Confirm Password');
		}
		else if(!pwd_expression.test(pwd))
		{
			alert ('Upper case, Lower case, Special character and Numeric letter are required in Password filed');
		}
		else if(pwd != cpwd)
		{
			alert ('Password not Matched');
		}
		else if(document.getElementById("confirmpassword").value.length < 6)
		{
			alert ('Password minimum length is 6');
		}
		else if(document.getElementById("confirmpassword").value.length > 12)
		{
			alert ('Password max length is 12');
		}
		else
		{				                            
			   alert('Thank You for Login ');
			   var name=$('#name').val();
				var username=$('#username').val();
				var password=$('#password').val();
				// var confirm_password=$('#pass2').val();
				var email=$('#email').val();
				// alert(code+input+language);
				$.ajax({
					url: '/registerUser',
					// data:"{'code':"+code+",'input':"+input+",'language':"+language+"}",
					data:{
						'name':name,
						'username':username,
						'password':password,
						// 'pass2':confirm_password,
						'email':email
					},
					type: 'POST',
					success: function(response) {
						console.log(response);
						var resp=JSON.parse(response);
						// $('#output').val(resp.output)
					},
					error: function(error) {
						console.log(error);
					}
				});
			   
		}
	}