$(function() {
    $('#compileandrun').click(function() { 
        var code=$('#exampleFormControlTextarea1').val();
        var input=$("#input").val();
        var language=$("#language").val();
        // alert(code+input+language);
        $.ajax({
            url: '/compileandrun',
            // data:"{'code':"+code+",'input':"+input+",'language':"+language+"}",
            data:{
                'code':code,
                'input':input,
                'language':language
            },
            type: 'POST',
            success: function(response) {
                console.log(response);
                var resp=JSON.parse(response);
                $('#output').val(resp.output)
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});
