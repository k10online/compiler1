from pymongo import MongoClient
from datetime import datetime
#setting up client
client = MongoClient('localhost', 27017)
#getting instance of client to create database
db=client['wbide_db']
#defining a collection

def createNewUser(_user,_pass,_email):
    user_collection=db['user_collection']
    data={
        'username':_user,
        'password':_pass,
        'email':_email
    }
    #inserting one data and getting id of the inserted data
    # post_id=user_collection.insert_one(data).inserted_id
    value=user_collection.find_one(data);
    # print(value)    
    if not value:
        post_id=user_collection.insert_one(data).inserted_id
        return True
    else:
        return False

def checkLogin(_username,_password):
    user_collection=db['user_collection']
    data={
        'username':_username,
        'password':_password
    }
    value=user_collection.find_one(data)
    # print('DB',value)
    if value != None:
        # print('yes')
        return True
    else:
        # print('no')
        return False

def checkUsername(_username):
    user_collection=db['user_collection']
    data={
        'username':_username,
    }
    value=user_collection.find_one(data)
    # print('DB',value)
    if value != None:
        # print('yes')
        return True
    else:
        # print('no')
        return False

def writeNewCode(_username,_filename,_code,_input):
    code_collection=db['code_collection']
    view_collection=db['view_collection']
    _instance=0
    
    data={
        'username':_username,
        'filename':_filename
    }
    
    value=view_collection.find_one(data)
    print('value ',value)
    
    inst=''
    fname=''
    
    if value != None:
        inst=value['lastfileinstance']
        fname=value['filename']

    if inst=='' and fname=='':
        _instance=0
    else:
        _instance=inst+1
    
    data={
        'username':_username,
        'filename':_filename,
        'fileinstance':_instance,
        'code':_code,
        'input':_input
    }

    key={
        'username':_username,
        'filename':_filename
    }
    insertData={
        'username':_username,
        'filename':_filename,
        'lastfileinstance':_instance
    }
    updateData={
        '$set':{
            'username':_username,
            'filename':_filename,
            'lastfileinstance':_instance
        }
    }
    value_present=view_collection.find_one(key)
    view_id=''
    if value_present == None:
        view_id=view_collection.insert_one(insertData)
    else:
        view_id=view_collection.update_one(key,updateData)

    # view_id=view_collection.update_one(key,updateData)
    print(view_id)
    post_id=code_collection.insert_one(data).inserted_id
    print(post_id)


def viewAllFiles(_username):
    code_collection=db['code_collection']
    view_collection=db['view_collection']
    return_list=[]
    data={
        'username':_username,
    }

    value=code_collection.find(data)
    # print(type(value))
    for val in value:
        # print(val)
        val['_id']=str(val['_id'])
        return_list.append(val)
    # print(return_list)
    return return_list

def viewAllInstancesOfFile(_username,_filename):
    code_collection=db['code_collection']
    view_collection=db['view_collection']

    data={
        'username':_username,
        'filename':_filename
    }

    value=code_collection.find(data)
    # print(value)
    for val in value:
        print(val)

#createNewUser('ketan','nopass','email')

# writeNewCode('amol',
# 'codepp.c',
# '#include<iostream>\n'+
# 'using namespace std;\n'+
# 'int main(){\n'+
# 'cout<<"hello123";\n'+
# 'return 0;\n'+
# '}',
# '2 3 4',
# datetime.now())

# checkLogin('e','k')

# viewAllInstancesOfFile('ketan','code.c')
viewAllFiles('amol')