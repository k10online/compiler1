from flask import Flask, render_template, json, request,redirect,url_for
import os
import compileFile as cf
import mongo_calls as db

def write_to_file(code,input,ext):
    with open('main'+ext,'w') as file:
        file.write(code)
    file.close()
    with open('input.txt','w') as file:
        file.write(input)
    file.close()

app = Flask(__name__)

#unregistered user landing page
@app.route('/')
def main():
    return render_template('html/registration.html')

@app.route('/home')
def home():
    return redirect(url_for('main'))

#unregistred user aboutus page
@app.route('/aboutuspage')
def aboutus():
    return render_template('html/aboutuspage.html')

#unregistered user help page
@app.route('/helppage')
def help():
    return render_template('html/helppage.html')

#all about registered user page redirections
@app.route('/useraboutuspage/<name>')
def useraboutus(name=None):
    if(db.checkUsername(name)):
        return render_template('html/useraboutuspage.html',name=name)

@app.route('/userhelppage/<name>')
def userhelp(name=None):
    if db.checkUsername(name):
        return render_template('html/userhelppage.html',name=name)

@app.route('/historypage/<name>')
def userhistorypage(name=None):
    if db.checkUsername(name): 
        return render_template('html/historypage.html',name=name)

@app.route('/filespage/<name>')
def userfilespage(name=None):
    if db.checkUsername(name):
        return render_template('html/filespage.html',name=name)

@app.route('/userpage/<username>')
def userpage(username=None):
    if db.checkUsername(username):
        return render_template('html/userpage.html',name=username)

@app.route('/saveFile',methods=['POST'])
def saveFile():
    _username=request.form['username']
    _filename=request.form['filename']
    _input=request.form['input']
    _code=request.form['code']
    print(_username,_filename,_code,_input)
    if _username and _filename and _code:
        db.writeNewCode(_username,_filename,_code,_input)
        return json.dumps({"response":"True"})
    else:
        return json.dumps({"response":"False"})

@app.route('/fetchFileList',methods=['POST'])
def fetchlist():
    _username=request.form['username']
    if _username:
        file_list=db.viewAllFiles(_username)
        return json.dumps({"response":file_list})
    else:
        return json.dumps({"response":"False"})

#register new user
@app.route('/registerUser',methods=['POST'])
def registerNewUser():
    _name=request.form['name']
    _username=request.form['username']
    _email=request.form['email']
    _password=request.form['password']
    
    #databse code for registration
    if db.createNewUser(_username,_password,_email):
        return json.dumps({"response":"registration successful"})
    else:
        return json.dumps({"response":"registration not successful"})


#logout the registered user
@app.route('/logout')
def logout():
    return redirect(url_for('main'))

#login the user
@app.route('/loginUser',methods=['POST'])
def loginUser():
    _username=request.form['username']
    _password=request.form['password']
    
    if _username and _password:
        val=db.checkLogin(_username,_password)
        print('API',val)
        if val == True:
            return json.dumps({"response":"True"})
        else:
            return json.dumps({"response":"False"})
    else:
        return json.dumps({"response":"Error"})
        

#main compiler logic
@app.route('/compileandrun',methods=['POST'])
def compileandrun(): 
    # read the posted values from the UI
    
    _code = request.form['code']
    _input = request.form['input']
    _language=request.form['language']
    # _code = request.form.get('code')
    # _input = request.form.get('input')
    # _language=request.form.get('language')
    _output=''
    if _code and _language:
        # return json.dumps({'html':'All fields good !!'})
        if(_language=='c'):
            write_to_file(_code,_input,'.c')
        elif(_language=='cpp'):
            write_to_file(_code,_input,'.cpp')
        _output=cf.compiler('main.'+_language,'input.txt')
        return json.dumps({'code':_code,'input':_input,'output':_output,'language':_language})
    else:
        return json.dumps({'html':'<span>Enter the required fields</span>'+_code+'  '+_input+'  '+_language})

if __name__ == "__main__":
    app.run()
